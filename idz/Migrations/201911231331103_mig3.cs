namespace idz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Номер_гостиницы", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Номер_гостиницы", "Image");
        }
    }
}
