namespace idz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.String(),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Вид_животного",
                c => new
                    {
                        Код_вида = c.Int(nullable: false, identity: true),
                        Наименование = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Код_вида);
            
            CreateTable(
                "dbo.Владелец_питомец",
                c => new
                    {
                        Код_клиента = c.Int(nullable: false),
                        Код_питомца = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Код_клиента, t.Код_питомца });
            
            CreateTable(
                "dbo.Возможность_выполнения_ухода",
                c => new
                    {
                        Код_сотркдника = c.Int(nullable: false),
                        Код_услуги = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Код_сотркдника, t.Код_услуги });
            
            CreateTable(
                "dbo.Журнал_ухода",
                c => new
                    {
                        Номер_услуги = c.Int(nullable: false),
                        Номер_заказа = c.Int(nullable: false),
                        Код_сотрудника = c.Int(nullable: false),
                        Код_заказанной_услуги = c.Int(),
                        Код_выполненной_услуги = c.Int(nullable: false),
                        Планируемаядата = c.DateTime(name: "Планируемая дата", storeType: "date"),
                        Фактическаядата = c.DateTime(name: "Фактическая дата", storeType: "date"),
                    })
                .PrimaryKey(t => new { t.Номер_услуги, t.Номер_заказа });
            
            CreateTable(
                "dbo.Заказ",
                c => new
                    {
                        Номер_заказа = c.Int(nullable: false, identity: true),
                        Срок_предоставления_услуги = c.Int(nullable: false),
                        Датазаказа = c.DateTime(name: "Дата заказа"),
                        Датаоплаты = c.DateTime(name: "Дата оплаты", storeType: "date"),
                        Код_питомца = c.Int(nullable: false),
                        Код_номера = c.Int(nullable: false),
                        Код_клиента = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Номер_заказа);
            
            CreateTable(
                "dbo.Клиент",
                c => new
                    {
                        Код_клиента = c.Int(nullable: false, identity: true),
                        ФИО = c.String(nullable: false, maxLength: 30),
                        Телефон = c.String(maxLength: 12),
                        Паспорт = c.String(maxLength: 50),
                        Почта = c.String(maxLength: 20),
                        Адрес = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Код_клиента);
            
            CreateTable(
                "dbo.Номер_гостиницы",
                c => new
                    {
                        Код_номера = c.Int(nullable: false, identity: true),
                        Количество_мест = c.Int(nullable: false),
                        Стоимость = c.Double(),
                        Код_типа_номера = c.Int(nullable: false),
                        Код_вида = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Код_номера);
            
            CreateTable(
                "dbo.Питомец",
                c => new
                    {
                        Кодпитомца = c.Int(name: "Код питомца", nullable: false, identity: true),
                        Кличка = c.String(nullable: false, maxLength: 15),
                        Возраст = c.Int(nullable: false),
                        Вес = c.Int(nullable: false),
                        Примечания = c.String(),
                        Код_животного = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Кодпитомца);
            
            CreateTable(
                "dbo.Сотрудник",
                c => new
                    {
                        Код_сотркдника = c.Int(nullable: false, identity: true),
                        ФИО = c.String(nullable: false, maxLength: 30),
                        Телефон = c.String(maxLength: 12),
                        Паспорт = c.String(maxLength: 50),
                        Квалификация = c.String(maxLength: 20),
                        Адрес = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Код_сотркдника);
            
            CreateTable(
                "dbo.Тип_номера",
                c => new
                    {
                        Код_типа_номера = c.Int(nullable: false, identity: true),
                        Наименование = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Код_типа_номера);
            
            CreateTable(
                "dbo.Услуга",
                c => new
                    {
                        Код_услуги = c.Int(nullable: false, identity: true),
                        Наименование = c.String(nullable: false, maxLength: 20),
                        Стоимость = c.Double(),
                    })
                .PrimaryKey(t => t.Код_услуги);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropTable("dbo.Услуга");
            DropTable("dbo.Тип_номера");
            DropTable("dbo.Сотрудник");
            DropTable("dbo.Питомец");
            DropTable("dbo.Номер_гостиницы");
            DropTable("dbo.Клиент");
            DropTable("dbo.Заказ");
            DropTable("dbo.Журнал_ухода");
            DropTable("dbo.Возможность_выполнения_ухода");
            DropTable("dbo.Владелец_питомец");
            DropTable("dbo.Вид_животного");
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
        }
    }
}
