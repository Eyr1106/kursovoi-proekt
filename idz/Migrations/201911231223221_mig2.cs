namespace idz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Сотрудник", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Сотрудник", "Image");
        }
    }
}
