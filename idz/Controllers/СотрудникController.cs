﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class СотрудникController : Controller
    {
        private PetsContextDB db = new PetsContextDB();


        //public ActionResult otdel()
        //{
        //    List<специальность> c = (from s in db.Сотрудники
        //                             select new специальность {название=s.Квалификация}).Distinct().ToList();

            

        //    return View(c);
        //}
        // GET: Сотрудник
        public ActionResult Index(string sot, string first_name)
        {

            IQueryable<Сотрудник> сотрудник = db.Сотрудники;
            if (sot != null && !sot.Equals("Все"))
            {
                сотрудник = сотрудник.Where(p => p.Квалификация == sot);
            }

            //List<Сотрудник> спецСотр = (from s in db.Сотрудники
            //                            select new Сотрудник { Квалификация = s.Квалификация }).Distinct().ToList();

            if (first_name != null && first_name != "")
            {
                string prov = first_name.Substring(0, first_name.Length - 2);
                сотрудник = сотрудник.Where(p => p.ФИО.Substring(0, p.ФИО.Length - 5).Contains (prov) /*== first_name*/);
            }



            var спецСотр =
            (from s in db.Сотрудники
             group s by new { s.Квалификация} into p
            select new 
 {
     Квалификация= p.Key.Квалификация
 }
).ToList();
            спецСотр.Insert(0, new { Квалификация = "Все"});

            Специальности специальность = new Специальности
            {
                Сотрудники = сотрудник.ToList(),
                СписокСпециальностей = new SelectList (спецСотр, "Квалификация", "Квалификация")
                   
            };
            //    СписокСпециальностей = (from s in db.Сотрудники
            //                            select new специальность { название = s.Квалификация }).Distinct().ToList()
            //        .Select(и => new SelectListItem()
            //        {
            //            Text = и.название,
            //            Value = и.название.ToString()
            //        })
            //        .ToList(),
            //};


            return View(специальность);
        }

        // GET: Сотрудник/Details/5
        [Authorize(Roles = "admin, user, worker")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудники.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // GET: Сотрудник/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Сотрудник/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Код_сотркдника,ФИО,Телефон,Паспорт,Квалификация,Адрес,Image")] Сотрудник сотрудник, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            { 
                byte[] imageData = null;
                // считываем переданный файл в массив байтов
                using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                {
                    imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
                }
                // установка массива байтов
                сотрудник.Image = imageData;
                db.Сотрудники.Add(сотрудник);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(сотрудник);
        }

        // GET: Сотрудник/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудники.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // POST: Сотрудник/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Код_сотркдника,ФИО,Телефон,Паспорт,Квалификация,Адрес")] Сотрудник сотрудник)
        {
            if (ModelState.IsValid)
            {
                db.Entry(сотрудник).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(сотрудник);
        }

        // GET: Сотрудник/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудники.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // POST: Сотрудник/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Сотрудник сотрудник = db.Сотрудники.Find(id);
            db.Сотрудники.Remove(сотрудник);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
