﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class НомераController : Controller
    {

        private PetsContextDB db = new PetsContextDB();
        // GET: Номера
        public ActionResult Index(string pet)
        {
           

            List<Номера_с> номер =
                (from n in db.Номера
                 join v in db.Виды_животного on n.Код_вида equals v.Код_вида
                 join tn in db.Типы_номеров on n.Код_типа_номера equals tn.Код_типа_номера

                 select new Номера_с
                 {  Image = n.Image,
                     Номер = n.Код_номера,
                     Стоимость = n.Стоимость.Value,
                     Количество_мест = n.Количество_мест,
                     Животное = v.Наименование,
                     Тип_номера = tn.Наименование,
                     Количество_заказов = (from z in db.Заказы
                                           where z.Код_номера == n.Код_номера
                                           select new { z.Номер_заказа}).Count()
                  }).ToList();


            if (pet != null && !pet.Equals("Все"))
            {
                номер = номер.Where(p => p.Животное == pet).ToList();
            }


            var животные =
           (from s in db.Виды_животного
            select new
            {
               Животное = s.Наименование
            }).ToList();
            животные.Insert(0, new { Животное = "Все" });

            Номера_с N = new Номера_с
            {
                НомераДляВыбора = номер.ToList(),
                СписокВыбораЖивотных = new SelectList(животные, "Животное", "Животное")

            };

            return View(N);
        }
    

        public ActionResult Create()
        {
            var номер = new Номера_с
            {
                СписокЖивотных = db.Виды_животного.ToList()
                    .Select(и => new SelectListItem()
                    {
                        Text = и.Наименование,
                        Value = и.Код_вида.ToString()
                    })
                    .ToList(),
                СписокНомеров = db.Типы_номеров.ToList()
                    .Select(и => new SelectListItem()
                    {
                        Text = и.Наименование,
                        Value = и.Код_типа_номера.ToString()
                    })
                    .ToList(),
            };
            return View(номер);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Номера_с n, HttpPostedFileBase uploadImage)
        {
            byte[] imageData = null;
            // считываем переданный файл в массив байтов
            using (var binaryReader = new BinaryReader(uploadImage.InputStream))
            {
                imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
            }

            var новый_номер = new Номер_гостиницы
            {
                //Код_номера = ,
                Количество_мест = n.Количество_мест,
                Стоимость = n.Стоимость,
                Код_вида = n.ID_вида,
                Код_типа_номера = n.ID_типа_н,
                Image = imageData
            };
            db.Номера.Add(новый_номер);

            db.SaveChanges();
            return RedirectToAction("Index");
        }



        public ActionResult Edit(int? id)
        {

            var номер_на_изменение = db.Номера.Find(id);
            List<SelectListItem> СписокЖивотныхБД = db.Виды_животного.ToList()
                .Select(и => new SelectListItem()
                {
                    Text = и.Наименование,
                    Value = и.Код_вида.ToString(),
                    Selected = и.Код_вида == номер_на_изменение.Код_вида
                })
                .ToList();
            List<SelectListItem> СписокНомеровБД = db.Типы_номеров.ToList()
                .Select(к => new SelectListItem()
                {
                    Text = к.Наименование,
                    Value = к.Код_типа_номера.ToString(),
                    Selected = к.Код_типа_номера == номер_на_изменение.Код_типа_номера

                })
                .ToList();

            var номер_для_изм = new Номера_с
            {    Номер= номер_на_изменение.Код_номера,
                ID_типа_н = номер_на_изменение.Код_типа_номера,
                ID_вида = номер_на_изменение.Код_вида,
                Количество_мест = номер_на_изменение.Количество_мест,
                Стоимость = номер_на_изменение.Стоимость.Value,
                СписокЖивотных = СписокЖивотныхБД,
                СписокНомеров = СписокНомеровБД,
                Image=номер_на_изменение.Image
            };
          
            return View(номер_для_изм);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Номера_с номер_для_изм, HttpPostedFileBase uploadImage)

        {
            byte[] imageData = null;
            // считываем переданный файл в массив байтов
            using (var binaryReader = new BinaryReader(uploadImage.InputStream))
            {
                imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
            }


            var номерНовый = db.Номера.Find(номер_для_изм.Номер);
            номерНовый.Стоимость = номер_для_изм.Стоимость;
            номерНовый.Количество_мест = номер_для_изм.Количество_мест;
            номерНовый.Код_вида = номер_для_изм.ID_вида;
            номерНовый.Код_типа_номера = номер_для_изм.ID_типа_н;
            номерНовый.Image = imageData;
            //db.SaveChanges();
            //return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                db.Entry(номерНовый).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ID_персоны = new SelectList(db.Игроки, "ID_персоны", "ID_персоны", контракт_игрока_с_клубом.ID_персоны);
            // ViewBag.ID_клуба = new SelectList(db.Клубы, "ID_клуба", "Название", контракт_игрока_с_клубом.ID_клуба);
            //ViewBag.ID_позиции = new SelectList(db.Позиции_игроков, "ID_позиции", "Название", контракт_игрока_с_клубом.ID_позиции);
            return View(номер_для_изм);


            //ViewBag.ID_персоны = new SelectList(db.Игроки, "ID_персоны", "ID_персоны", контракт_игрока_с_клубом.ID_персоны);
            // ViewBag.ID_клуба = new SelectList(db.Клубы, "ID_клуба", "Название", контракт_игрока_с_клубом.ID_клуба);
            //ViewBag.ID_позиции = new SelectList(db.Позиции_игроков, "ID_позиции", "Название", контракт_игрока_с_клубом.ID_позиции);
            //return View(контрактИгрока);
        }
    }
}