﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class УслугаController : Controller
    {
        private PetsContextDB db = new PetsContextDB();

        // GET: Услуга
        public ActionResult Index()
        {
            return View(db.Услуги.ToList());
        }

        // GET: Услуга/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Услуга услуга = db.Услуги.Find(id);
            if (услуга == null)
            {
                return HttpNotFound();
            }
            return View(услуга);
        }

        // GET: Услуга/Create
        //[Authorize(Roles = "user")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Услуга/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Код_услуги,Наименование,Стоимость")] Услуга услуга)
        {
            if (ModelState.IsValid)
            {
                db.Услуги.Add(услуга);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(услуга);
        }

        // GET: Услуга/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Услуга услуга = db.Услуги.Find(id);
            if (услуга == null)
            {
                return HttpNotFound();
            }
            return View(услуга);
        }

        // POST: Услуга/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Код_услуги,Наименование,Стоимость")] Услуга услуга)
        {
            if (ModelState.IsValid)
            {
                db.Entry(услуга).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(услуга);
        }

        // GET: Услуга/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Услуга услуга = db.Услуги.Find(id);
            if (услуга == null)
            {
                return HttpNotFound();
            }
            return View(услуга);
        }

        // POST: Услуга/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Услуга услуга = db.Услуги.Find(id);
            db.Услуги.Remove(услуга);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
