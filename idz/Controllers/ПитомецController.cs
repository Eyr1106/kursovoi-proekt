﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class ПитомецController : Controller
    {
        private PetsContextDB db = new PetsContextDB();

        // GET: Питомец
        public ActionResult Index()
        {
            return View(db.Питомцы.ToList());
        }

        // GET: Питомец/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Питомец питомец = db.Питомцы.Find(id);
            if (питомец == null)
            {
                return HttpNotFound();
            }
            return View(питомец);
        }

        // GET: Питомец/Create
        public ActionResult Create(int? id)
        {
            var клиенты_изм = new Владельцы
            {
            СписокКлиентов= (from м in db.Клиенты

                             where м.Код_клиента == id
                             select new SelectListItem { Text = м.ФИО, Value = м.Код_клиента.ToString() }).ToList(),
              
           СписокВидовЖивотных  = db.Виды_животного.ToList()
                .Select(и => new SelectListItem()
                {
                    Text = и.Наименование,
                    Value = и.Код_вида.ToString()

                })
                .ToList()
        };

        //if (id != null)
        //{
        //    ViewBag.MyProperty = id.Value;
        //    return View();
        //}
        //var клиенты_изм = new Владельцы
        //    {
        //       СписокВидовЖивотных = СписокВидовДБ,
        //       СписокКлиентов = СписокКлиентовДБ
        //    };

            return View(клиенты_изм);
        }

        // POST: Питомец/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Владельцы к, bool? newPet)
        {

            var новый_питомец = new Питомец
            {
                //Код_номера = ,
               Кличка = к.Кличка,
                Вес = к.Вес,
               Возраст = к.Возраст,
                Примечания = к.Примечания,
                Код_животного =к.Код_животного

            };
            db.Питомцы.Add(новый_питомец);
            db.SaveChanges();
            //var t = db.Питомцы.Last();
            var t = db.Питомцы.Max(a => a.Код_питомца);
            
            var новая_связь = new Владелец_питомец
            {Код_клиента = к.Код_владельца,
            Код_питомца = t
            };
            db.Владельцы_Питомцы.Add(новая_связь);
            db.SaveChanges();
            if (newPet != null && newPet.Value == true)
            {
                return RedirectToAction("Create", "Заказ_клиента",  new { id = к.Код_владельца });
            }
            return RedirectToAction("Create", "Заказ_клиента", new { id = к.Код_владельца });
        }

        // GET: Питомец/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Питомец питомец = db.Питомцы.Find(id);
            if (питомец == null)
            {
                return HttpNotFound();
            }
            return View(питомец);
        }

        // POST: Питомец/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Код_питомца,Кличка,Возраст,Вес,Примечания,Код_животного")] Питомец питомец)
        {
            if (ModelState.IsValid)
            {
                db.Entry(питомец).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(питомец);
        }

        // GET: Питомец/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Питомец питомец = db.Питомцы.Find(id);
            if (питомец == null)
            {
                return HttpNotFound();
            }
            return View(питомец);
        }

        // POST: Питомец/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Питомец питомец = db.Питомцы.Find(id);
            db.Питомцы.Remove(питомец);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
