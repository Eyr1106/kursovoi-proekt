﻿using idz.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace idz.Controllers
{
    public class ConsoleController : Controller
    {
        private PetsContextDB db = new PetsContextDB();
        // GET: Console
        public ActionResult Index(string US)
        {
            List<User_Cons> user =
               (from n in db.Users
                join v in db.Roles on n.RoleId equals v.Id
               orderby v.Name
            

                select new User_Cons
                {
                    Id = n.Id,
                    Email = n.Email,
                    Password = n.Password,
                    Role = v.Name
                  
                }).ToList();
            if (US != null && !US.Equals("Все"))
            {
                user = user.Where(p => p.Role == US).ToList();
            }
            var us11 =
         (from s in db.Roles
          select new
          {
              R = s.Name
          }).ToList();
            us11.Insert(0, new { R = "Все" });

            User_Cons N = new User_Cons
            {
               ДляВыбора = user.ToList(),
            СписокRole = new SelectList(us11, "R", "R")
            };

            return View(N);
        }

        public ActionResult Edit(int? id)
        {

            var пользователь = db.Users.Find(id);
            List<SelectListItem> Roles = db.Roles.ToList()
                .Select(и => new SelectListItem()
                {
                    Text = и.Name,
                    Value = и.Id.ToString(),
                    Selected = и.Id == пользователь.RoleId
                })
                .ToList();
           

            var пользователь2 = new User_Cons
            {
                Id = пользователь.Id,
                Email = пользователь.Email,
                Password = пользователь.Password,
                ID_role=пользователь.RoleId,
                СписокРолей = Roles

            };

            return View(пользователь2);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User_Cons us1)

        {
           

            var us2 = db.Users.Find(us1.Id);
           us2.Email = us1.Email;
            us2.Password = us1.Password;
            us2.RoleId = us1.ID_role;
           

            if (ModelState.IsValid)
            {
                db.Entry(us2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(us1);


        }

        public ActionResult Create()
        {
            var us1 = new User_Cons
            {
                СписокРолей = db.Roles.ToList()
                .Select(и => new SelectListItem()
                {
                    Text = и.Name,
                    Value = и.Id.ToString(),
                 
                })
                .ToList(),

            };
            return View(us1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User_Cons US)
        {
          

            var us2 = new User
            {    Email = US.Email,
            Password = US.Password,
            RoleId = US.ID_role

            };
            db.Users.Add(us2);

            db.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}