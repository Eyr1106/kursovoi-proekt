﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class ЖурналController : Controller
    {
        private PetsContextDB db = new PetsContextDB();
        // GET: Журнал
        public ActionResult Index()
        {

            List<Журнал> Журнал =
          (from z in db.Заказы
           join vp in db.Владельцы_Питомцы on new { z.Код_клиента, z.Код_питомца } equals new { vp.Код_клиента, vp.Код_питомца }
           join p in db.Питомцы on vp.Код_питомца equals p.Код_питомца
           join bu in db.Журналы on z.Номер_заказа equals bu.Номер_заказа
           //join vu in db.Возможности_ухода on new { bu.Код_выполненной_услуги, bu.Код_сотрудника } equals new { vu.Код_услуги, vu.Код_сотркдника }
           join u in db.Услуги on bu.Код_выполненной_услуги equals u.Код_услуги
           join s in db.Сотрудники on bu.Код_сотрудника equals s.Код_сотркдника
           select new Журнал
           {
               Номер_заказа = z.Номер_заказа,
               Номер_услуги = bu.Номер_услуги,
               Кличка = p.Кличка,
               ФИО_сотрудника = s.ФИО,
               Дата = bu.Фактическая_дата.Value,
               Услуга = u.Наименование
           }).ToList();

            return View(Журнал);

          
        }
    }
}