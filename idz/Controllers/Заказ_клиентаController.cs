﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class Заказ_клиентаController : Controller
    {
        // GET: Заказ_клиента

        private PetsContextDB db = new PetsContextDB();

        // GET: заказ_клиента
        [Authorize(Roles = "worker, admin")]
        public ActionResult Index()
        {
            var t = (from bu in db.Журналы
                     join u in db.Услуги on bu.Код_выполненной_услуги equals u.Код_услуги
                     join z in db.Заказы on bu.Номер_заказа equals z.Номер_заказа
                     group new { bu, u, z } by new { z.Код_клиента, z.Код_питомца } into pg
                     let name = pg.FirstOrDefault()
                     let заказ = name.z
                     let sum = pg.Sum(m => m.u.Стоимость)
                     select new {заказ.Номер_заказа, sum.Value }).First().Value;

            var x = (from zk in db.Заказы
                     join n in db.Номера on zk.Код_номера equals n.Код_номера
                     group new {zk, n } by new { zk.Код_клиента, zk.Код_питомца } into pg
                     let name = pg.FirstOrDefault()
                     let заказ = name.zk
                     let sum = pg.Sum(m => (m.n.Стоимость * m.zk.Срок_предоставления_услуги))
                     select new { заказ.Номер_заказа, sum.Value }).First().Value;
            //(from bu in db.Журналы
            // join u in db.Услуги on bu.Код_выполненной_услуги equals u.Код_услуги
            // join zk in db.Заказы on bu.Номер_заказа equals zk.Номер_заказа
            // where zk.Номер_заказа == z.Номер_заказа
            // group new { bu, u, zk } by new { zk.Код_клиента, zk.Код_питомца } into pg
            // let name = pg.FirstOrDefault()
            // let заказ = name.zk
            // let sum = pg.Sum(m => m.u.Стоимость)
            // select new { sum.Value }).FirstOrDefault().Value


            List<заказ_клиента> Заказ =
           (from z in db.Заказы
            join vp in db.Владельцы_Питомцы on new { z.Код_клиента, z.Код_питомца } equals new { vp.Код_клиента, vp.Код_питомца }
            join k in db.Клиенты on vp.Код_клиента equals k.Код_клиента
            //join bu in db.Журналы on z.Номер_заказа equals bu.Номер_заказа
            //join u in db.Услуги on bu.Код_заказанной_услуги equals u.Код_услуги
            select new заказ_клиента
            {
                Номер_заказа = z.Номер_заказа,
                Код_клиента = z.Код_клиента,
                ФИО = k.ФИО,
                Дата_заказа = z.Дата_заказа.Value,
               
            Сумма_услуг = (from bu in db.Журналы
                                join u in db.Услуги on bu.Код_выполненной_услуги equals u.Код_услуги
                                join zk in db.Заказы on bu.Номер_заказа equals zk.Номер_заказа
                                where zk.Номер_заказа == z.Номер_заказа
                                group new { bu, u, zk } by new { zk.Код_клиента, zk.Код_питомца } into pg
                                let name = pg.FirstOrDefault()
                                let заказ = name.zk
                                let sum = pg.Sum(m => m.u.Стоимость)
                                select new { sum.Value }).FirstOrDefault().Value,
            Стоимость_проживания = (from zk in db.Заказы
                                    where zk.Номер_заказа == z.Номер_заказа
                                    join n in db.Номера on zk.Код_номера equals n.Код_номера
                                    group new { zk, n } by new { zk.Код_клиента, zk.Код_питомца } into pg
                                    let name = pg.FirstOrDefault()
                                    let заказ = name.zk
                                    let sum = pg.Sum(m => (m.n.Стоимость * m.zk.Срок_предоставления_услуги))
                                    select new { sum.Value }).FirstOrDefault().Value

            }).ToList();

           заказ_клиента N = new заказ_клиента
            {
                ЗаказыДляВыбора = Заказ.ToList(),

            };

            return View(N);
        }
        [Authorize(Roles = "worker")]
        public ActionResult Details(int? id)
        {

           Заказ заказ = db.Заказы.Find(id);

            List<заказ_клиента> Заказ = (from z in db.Заказы
                                         where z.Номер_заказа == заказ.Номер_заказа
                                       
                                     join vp in db.Владельцы_Питомцы on new { z.Код_клиента, z.Код_питомца } equals new { vp.Код_клиента, vp.Код_питомца }
                                     join p in db.Питомцы on vp.Код_питомца equals p.Код_питомца
                                     join ve in db.Виды_животного on p.Код_животного equals ve.Код_вида
                                     join k in db.Клиенты on vp.Код_клиента equals k.Код_клиента
                                     join n in db.Номера on z.Код_номера equals n.Код_номера
                                     join tn in db.Типы_номеров on n.Код_типа_номера equals tn.Код_типа_номера
                                     select new заказ_клиента
                                     {
                                         Номер_заказа = z.Номер_заказа,
                                         Кличка = ve.Наименование+ " - "+ p.Кличка,
                                         ФИО = k.ФИО,
                                         Дата_заказа = z.Дата_заказа.Value,
                                         Дата_оплаты = z.Дата_оплаты.Value,
                                         Срок_предоставления_услуги = z.Срок_предоставления_услуги,
                                         Номер = z.Код_номера + " - "+tn.Наименование,
                                       
                                     }).ToList();
            

            заказ_клиента Z = new заказ_клиента
            {
                ЗаказыДляВыбора = Заказ.ToList()
            };
            //if (заказ == null)
            //{
            //    return HttpNotFound();
            //}
            return View(Z);
        }

        [Authorize(Roles = "worker")]
        public ActionResult Edit(int? id)
        {

            var заказ_измен = db.Заказы.Find(id);

            List<SelectListItem> СписокНомеровБД = (from n in db.Номера
                                                    join vp in db.Виды_животного on n.Код_вида equals vp.Код_вида
                                                    join p in db.Питомцы on vp.Код_вида equals p.Код_животного
                                                    where p.Код_питомца == заказ_измен.Код_питомца
                                                    join tn in db.Типы_номеров on n.Код_типа_номера equals tn.Код_типа_номера
                                                    select new SelectListItem()
                                                    {
                                                        Text = n.Код_номера + "-" + tn.Наименование,
                                                        Value = n.Код_типа_номера.ToString(),
                                                        Selected = n.Код_номера == заказ_измен.Код_номера
                                                    }).ToList();

            var заказ = new заказ_клиента
            {
               Номер_заказа = заказ_измен.Номер_заказа,
              Дата_заказа = заказ_измен.Дата_заказа.Value,
              Дата_оплаты = заказ_измен.Дата_оплаты.Value,
              ФИО = (from d in db.Заказы
                     where d.Номер_заказа == заказ_измен.Номер_заказа
                     join k in db.Клиенты on d.Код_клиента equals k.Код_клиента
                     select new { k.ФИО }).First().ФИО,
              Срок_предоставления_услуги = заказ_измен.Срок_предоставления_услуги,
              Кличка = (from d in db.Заказы
                        where d.Номер_заказа == заказ_измен.Номер_заказа
                        join k in db.Питомцы on d.Код_питомца equals k.Код_питомца
                        select new { k.Кличка }).First().Кличка,
              Код_клиента = заказ_измен.Код_клиента,
              Код_питомца = заказ_измен.Код_питомца,
                СписокНомеров = СписокНомеровБД
            };

            return View(заказ);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(заказ_клиента заказ)
        {
            var старыйЗаказ = db.Заказы.Find(заказ.Номер_заказа);
            старыйЗаказ.Дата_заказа = заказ.Дата_заказа;
            старыйЗаказ.Дата_оплаты = заказ.Дата_оплаты;
            старыйЗаказ.Код_номера = заказ.Код_номера;
            старыйЗаказ.Срок_предоставления_услуги = заказ.Срок_предоставления_услуги;
           
          if (ModelState.IsValid)
            {
                db.Entry(старыйЗаказ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(заказ);

            
        }

        public ActionResult ClientSelect()
        {
            var клиенты = new Владельцы
            {
                СписокКлиентов = db.Клиенты.ToList()
               .Select(и => new SelectListItem()
               {
                   Text = и.ФИО,
                   Value = и.Код_клиента.ToString()
               })
               .ToList(),


            };
            return View(клиенты);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientSelect(Владельцы в)
        {
            var NumberVlad = в.Код_владельца;


            return RedirectToAction("Create", new { id = NumberVlad });
        }



        public ActionResult Create(int? id)
        {
           

            var заказ = new заказ_клиента
            {
                СписокПитомцев = (from p in db.Питомцы
                                  join bp in db.Виды_животного on p.Код_животного equals bp.Код_вида
                                  join vp in db.Владельцы_Питомцы on p.Код_питомца equals vp.Код_питомца
                                  where vp.Код_клиента == id
                                  select new SelectListItem { Text =bp.Наименование+ " "+ p.Кличка, Value = p.Код_питомца.ToString()}).ToList(),
                
                СписокНомеров = (from n in db.Номера
                                 join bp in db.Виды_животного on n.Код_вида equals bp.Код_вида
                                 join tn in db.Типы_номеров on n.Код_типа_номера equals tn.Код_типа_номера
                                 
                                 select new SelectListItem { Text = n.Код_номера + "-" + tn.Наименование+ " номер для: "+ bp.Наименование,
                                                             Value = n.Код_номера.ToString() }).ToList(),
                Код_клиента = id.Value,
                ФИО =  (from k in db.Клиенты
                                where k.Код_клиента == id
                                select new { k.ФИО }).First().ФИО,

                СписокВладельцев = (from м in db.Клиенты
                                 
                                  where м.Код_клиента == id
                                  select new SelectListItem { Text = м.ФИО, Value = м.Код_клиента.ToString() }).ToList(),
                Дата_заказа= DateTime.Now,
                Дата_оплаты = DateTime.Now
            };
            return View(заказ);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(заказ_клиента n)
        {

            var новый_заказ = new Заказ
            {
                //Код_номера = ,
                Дата_заказа = n.Дата_заказа,
               Дата_оплаты = n.Дата_оплаты,
               Срок_предоставления_услуги = n.Срок_предоставления_услуги,
                Код_номера = n.Код_номера,
                Код_клиента = n.Код_клиента,
                Код_питомца = n.Код_питомца

            };
            db.Заказы.Add(новый_заказ);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}