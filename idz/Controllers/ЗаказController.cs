﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using idz.Models;

namespace idz.Controllers
{
    public class ЗаказController : Controller
    {
        private PetsContextDB db = new PetsContextDB();

        // GET: Заказ
        public ActionResult Index(string god)
        {

            List<Заказ> mon = db.Заказы.ToList();
            if (god != null)
            {
                mon = mon.Where(p => p.Дата_заказа.ToString().Split(' ')[0].Split('.')[2] == god.ToString()).ToList();
            }
            return View(mon);
        }

        // GET: Заказ/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Заказ заказ = db.Заказы.Find(id);
            if (заказ == null)
            {
                return HttpNotFound();
            }
            return View(заказ);
        }

        // GET: Заказ/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Заказ/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Номер_заказа,Срок_предоставления_услуги,Дата_заказа,Дата_оплаты,Код_питомца,Код_номера,Код_клиента")] Заказ заказ)
        {
            if (ModelState.IsValid)
            {
                db.Заказы.Add(заказ);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(заказ);
        }

        // GET: Заказ/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Заказ заказ = db.Заказы.Find(id);
            if (заказ == null)
            {
                return HttpNotFound();
            }
            return View(заказ);
        }

        // POST: Заказ/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Номер_заказа,Срок_предоставления_услуги,Дата_заказа,Дата_оплаты,Код_питомца,Код_номера,Код_клиента")] Заказ заказ)
        {
            if (ModelState.IsValid)
            {
                db.Entry(заказ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(заказ);
        }

        // GET: Заказ/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Заказ заказ = db.Заказы.Find(id);
            if (заказ == null)
            {
                return HttpNotFound();
            }
            return View(заказ);
        }

        // POST: Заказ/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Заказ заказ = db.Заказы.Find(id);
            db.Заказы.Remove(заказ);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
