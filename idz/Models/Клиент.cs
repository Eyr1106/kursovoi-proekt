﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Клиент")]
    public class Клиент
    {

        [Key]
        public int Код_клиента { get; set; }

        [Required]
        [StringLength(30)]
        public string ФИО { get; set; }

        [StringLength(12)]
        public string Телефон { get; set; }

        [StringLength(50)]
        public string Паспорт { get; set; }


        [StringLength(20)]
        public string Почта { get; set; }

        [StringLength(100)]
        public string Адрес { get; set; }

    }
}