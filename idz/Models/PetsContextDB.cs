﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace idz.Models
{
    public class PetsContextDB: DbContext

    {

       
        public  DbSet<Вид_животного> Виды_животного { get; set; }
        public  DbSet<Владелец_питомец> Владельцы_Питомцы { get; set; }
        public  DbSet<Возможность_выполнения_ухода> Возможности_ухода { get; set; }
        public virtual DbSet<Журнал_ухода> Журналы { get; set; }
        public virtual DbSet<Заказ> Заказы { get; set; }
        public virtual DbSet<Клиент> Клиенты { get; set; }
        public virtual DbSet<Номер_гостиницы> Номера { get; set; }
        public virtual DbSet<Питомец> Питомцы { get; set; }
        public virtual DbSet<Сотрудник> Сотрудники { get; set; }
        public virtual DbSet<Тип_номера> Типы_номеров { get; set; }
        public virtual DbSet<Услуга> Услуги { get; set; }
       
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

    }
}