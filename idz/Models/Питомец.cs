﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web.Mvc;

namespace idz.Models
{
    [Table("Питомец")]
    public partial class Питомец
    {
        [Key]
        [Column("Код питомца")]
        public int Код_питомца { get; set; }

        [Required]
        [StringLength(15)]
        public string Кличка { get; set; }

        public int Возраст { get; set; }
     

        public int Вес { get; set; }

       
        public string Примечания { get; set; }

       
        public int Код_животного { get; set; }

       

    }
}