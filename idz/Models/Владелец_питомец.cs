﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Владелец_питомец")]
    public class Владелец_питомец
    {
        [Key, Column(Order =1)]

        public int Код_клиента { get; set; }
        [Key, Column(Order = 2)]
        public int Код_питомца { get; set; }
    }
}