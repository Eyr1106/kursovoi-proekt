﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace idz.Models
{
    public class Журнал
    {
        public int Номер_заказа { get; set; }

        public int Номер_услуги { get; set; }

        public string Кличка { get; set; }

        public string ФИО_сотрудника { get; set; }

        public string Услуга { get; set; }

        public DateTime Дата { get; set; }


    }
}