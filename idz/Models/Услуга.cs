﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
namespace idz.Models
{
    [Table("Услуга")]
    public class Услуга
    {
        [Key]
        public int Код_услуги { get; set; }

        [Required]
        [StringLength(20)]
        public string Наименование { get; set; }

        public double? Стоимость { get; set; }

    }
}