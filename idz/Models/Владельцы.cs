﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace idz.Models
{
    public class Владельцы
    {
        public int Код_питомца { get; set; }

        
        public string Кличка { get; set; }

        public int Возраст { get; set; }

        public int Код_владельца { get; set; }

        public int Вес { get; set; }


        public string Примечания { get; set; }

        
        public int Код_животного { get; set; }

        public List<SelectListItem> СписокКлиентов { get; set; }
        public List<SelectListItem> СписокВидовЖивотных { get; set; }
    }
}