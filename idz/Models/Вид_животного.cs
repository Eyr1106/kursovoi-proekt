﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Вид_животного")]
    public class Вид_животного
    {
        [Key]
        public int Код_вида { get; set; }

        [Required]
        [StringLength(20)]
        public string Наименование { get; set; }


    }
}