﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Тип_номера")]
    public class Тип_номера
    {
        [Key]
        public int Код_типа_номера { get; set; }

        [Required]
        [StringLength(10)]
        public string Наименование { get; set; }
    }
}