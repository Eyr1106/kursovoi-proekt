﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Журнал_ухода")]
    public class Журнал_ухода
    {
        [Key, Column(Order = 1)]
        public int Номер_услуги { get; set; }

        [Key, Column(Order = 2)]
        public int Номер_заказа { get; set; }

        public int Код_сотрудника { get; set; }
        public int? Код_заказанной_услуги { get; set; }

        public int Код_выполненной_услуги { get; set; }

        [Column("Планируемая дата", TypeName = "date")]
        public DateTime? Планируемая_дата { get; set; }

        [Column("Фактическая дата", TypeName = "date")]
        public DateTime? Фактическая_дата { get; set; }

    }
}