﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace idz.Models
{
    public class Номера_с
    {
        public int Номер { get; set; }

        public double Стоимость { get; set; }

        public int Количество_мест { get; set; }

        public int ID_вида { get; set; }
        public string Животное { get; set; }

        public int ID_типа_н { get; set; }
        public string Тип_номера { get; set; }

        public byte[] Image { get; set; }

        public int Количество_заказов { get; set; }
        public List<SelectListItem> СписокЖивотных { get; set; }
        public List<SelectListItem> СписокНомеров { get; set; }

        public List<Номера_с> НомераДляВыбора { get; set; }
        public SelectList СписокВыбораЖивотных { get; set; }
    }
}