﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace idz.Models
{
    public class User_Cons
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public int ID_role { get; set; }
        public List<SelectListItem> СписокРолей { get; set; }
        public List<User_Cons> ДляВыбора { get; set; }
        public SelectList СписокRole { get; set; }
    }
}