﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Заказ")]
    public class Заказ
    {
        [Key]
        public int Номер_заказа { get; set; }

        public int Срок_предоставления_услуги { get; set; }
     
        [Column("Дата заказа")]
        public DateTime? Дата_заказа { get; set; }

        [Column("Дата оплаты", TypeName = "date")]
        public DateTime? Дата_оплаты { get; set; }

        public int Код_питомца { get; set; }
        public int Код_номера { get; set; }
        public int Код_клиента { get; set; }
    }
}