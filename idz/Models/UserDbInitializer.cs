﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace idz.Models
{
    public class UserDbInitializer: DropCreateDatabaseAlways<PetsContextDB>
    {
        protected override void Seed(PetsContextDB db)
        {
            Role admin = new Role { Name = "admin" };
            Role user = new Role { Name = "user" };
            db.Roles.Add(admin);
            db.Roles.Add(user);
            db.Users.Add(new User
            {
                Email = "admin@mail.ru",
                Password = "123456",
                Role = admin
            });
            base.Seed(db);
        }
    }
}