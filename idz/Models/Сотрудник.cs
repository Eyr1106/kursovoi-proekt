﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web.Mvc;

namespace idz.Models
{
    [Table("Сотрудник")]
    public class Сотрудник
    {

        [Key]
        public int Код_сотркдника { get; set; }

        [Required]
        [StringLength(30)]
        public string ФИО { get; set; }

        [StringLength(12)]
        public string Телефон { get; set; }

        [StringLength(50)]
        public string Паспорт { get; set; }


        [StringLength(20)]
        public string Квалификация { get; set; }

        [StringLength(100)]
        public string Адрес { get; set; }

        public byte[] Image { get; set; }

    }
}