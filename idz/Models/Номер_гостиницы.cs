﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Номер_гостиницы")]
    public class Номер_гостиницы
    {
        [Key]
        public int Код_номера { get; set; }

        
        public int Количество_мест{ get; set; }

        public double? Стоимость { get; set; }

        public int Код_типа_номера { get; set; }

        public int Код_вида { get; set; }

        public byte[] Image { get; set; }
    }
}