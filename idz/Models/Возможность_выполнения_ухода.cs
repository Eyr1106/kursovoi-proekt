﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace idz.Models
{
    [Table("Возможность_выполнения_ухода")]
    public class Возможность_выполнения_ухода
    {
        [Key, Column(Order = 1)]
        public int Код_сотркдника { get; set; }

        [Key, Column(Order = 2)]
        public int Код_услуги { get; set; }
    }
}