﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web.Mvc;

namespace idz.Models
{
 
    public class заказ_клиента
    {
        
        public int Номер_заказа { get; set; }
      
        public string ФИО { get; set; }

        public int Код_клиента { get; set; }

        public string Кличка { get; set; }
        public int Код_номера { get; set; }

        public string Номер { get; set; }

        public bool новыйПитомецВладельца { get; set; }

        public int Код_питомца { get; set; }
        public int Срок_предоставления_услуги { get; set; }
        public DateTime Дата_заказа { get; set; }
        public DateTime Дата_оплаты { get; set; }

        public double? Сумма_услуг { get; set; }
        public double? Стоимость_проживания { get; set; }

        public List<SelectListItem> СписокНомеров { get; set; }
        public List<SelectListItem> СписокПитомцев { get; set; }
        public List<SelectListItem> СписокВладельцев { get; set; }
        public List<заказ_клиента> ЗаказыДляВыбора { get; set; }

    }
}